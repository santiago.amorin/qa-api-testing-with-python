import requests


class ApiPets:

    def base_url(self):
        base_url = "https://petstore.swagger.io/v2/pet/"
        return base_url

    def header(self):
        header = {'Accept': 'application/json'}
        return header

    def body(self, pet_id, pet_name):
        return {
          "id": pet_id,
          "category": {
            "id": pet_id,
            "name": pet_name
          },
          "name": pet_name,
          "photoUrls": [
            "string"
          ],
          "tags": [
            {
              "id": pet_id,
              "name": "string"
            }
          ],
          "status": "available"}

    def get_pet_api(self, pet_id):
        return requests.get(url=self.base_url() + pet_id)

    def post_pet_api(self, body):
        return requests.post(url=self.base_url(), json=body)

    def get_mocked_response(self):
        expected = {
            "id": 1,
            "category": {
                "id": 1
            },
            "name": "Mocked name",
            "photoUrls": [],
            "tags": [],
            "status": "available"
        }
        return expected