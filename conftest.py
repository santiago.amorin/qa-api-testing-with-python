import logging
import os
import pytest
import faker
import random
from src.api.api_pets import ApiPets


def pytest_addoption(parser):
    parser.addoption('--env', action='store', default='', help='')
    parser.addoption('--role', action='store', default='', help='"')


def pytest_configure(config):
    os.environ["env"] = config.getoption('env')
    os.environ["role"] = config.getoption('role')


@pytest.fixture(scope="function")
def generate_rand_name():
    """Fixture. Generate random pet name"""
    fake = faker.Faker()
    pet_name = fake.name()
    return pet_name


@pytest.fixture(scope="function")
def generate_rand_id():
    """Fixture. Generate random pet id"""
    random_id = str(random.randint(15, 80))
    return random_id


@pytest.fixture(scope="function")
def generate_pet(generate_rand_id, generate_rand_name):
    """Fixture. Generate a new pet and return the generated id as a string"""
    api = ApiPets()
    body = api.body(pet_id=generate_rand_id, pet_name=generate_rand_name)
    response = api.post_pet_api(body=body)
    yield str(response.json()['id'])


@pytest.fixture(scope="function")
def generate_pet_id_1(generate_rand_name):
    """Fixture. Generate a new pet with pet id = 1"""
    api = ApiPets()
    body = api.body(pet_id='1', pet_name=generate_rand_name)
    response = api.post_pet_api(body=body)
    yield str(response.json()['id'])
