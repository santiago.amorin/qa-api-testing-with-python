import requests
from src.api.api_pets import ApiPets


class TestMockedApi():

    def setup(self):
        self.apis = ApiPets()
        self.pet_id = '1'

    def test_mock(self, requests_mock):
        """Test example using a mock for a get request"""
        expected = self.apis.get_mocked_response()
        requests_mock.get(url=self.apis.base_url() + self.pet_id, json=expected, status_code=201)
        response = requests.get(url=self.apis.base_url() + self.pet_id)
        print(response)
        print(response.text)
        print(response.json())
        assert response.status_code == 201

    def test_without_mock(self):
        """Test example without mock to see the difference between the responses"""
        response = requests.get(url=self.apis.base_url() + self.pet_id)
        print(response)
        print(response.text)
        print(response.json())
        assert response.status_code == 200