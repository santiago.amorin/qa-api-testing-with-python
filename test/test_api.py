import pytest
import requests
from assertpy import assert_that, soft_assertions
import logging
from src.api.api_pets import ApiPets



class Tests():

    def setup_class(self):
        self.apis = ApiPets()
        self.logging = logging.getLogger(__name__)

    @pytest.mark.critical
    def test_post_pet(self, generate_rand_id, generate_rand_name):
        """Test Post Method. Generate a new pet and verify that the response status code is 200"""
        self.logging.info("Generating a new pet using post method...")
        self.body = self.apis.body(pet_id=generate_rand_id, pet_name=generate_rand_name)
        response = self.apis.post_pet_api(body=self.body)
        assert response.status_code == 200

    @pytest.mark.critical
    def test_get_pet(self, generate_pet):
        """Test Get Method. Generate a new pet (using pytest fixture) and verify that the response
        status code is 200, and that the pet id returned in the response is equal to the id of the generated pet"""
        self.logging.info("Getting an existing pet using get method...")
        response = self.apis.get_pet_api(pet_id=generate_pet)
        print(response.json())
        assert response.status_code == 200
        assert generate_pet == str(response.json()['id'])

    def test_get_pet_assertpy(self, generate_pet_id_1):
        """Example using assertpy assertions"""
        response = self.apis.get_pet_api(pet_id=generate_pet_id_1)
        response_dict = response.json()
        # Converts a Python dictionary into a Python list of dictionaries to use the assertpy assertions.
        response_list_dict = [response.json()]
        assert_that(response.status_code).is_equal_to(requests.codes.ok)
        assert_that(response_dict).is_not_empty()
        assert_that(response_list_dict).extracting('status').contains('available')